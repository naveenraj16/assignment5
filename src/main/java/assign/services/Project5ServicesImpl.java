package assign.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import assign.domain.Meeting;
import assign.domain.Project;

public class Project5ServicesImpl implements Project5Services {
	
	String dbURL = "";
	String dbUsername = "";
	String dbPassword = "";
	DataSource ds;
	
	// DB connection information would typically be read from a config file.
	public Project5ServicesImpl(String dbUrl, String username, String password) {
		this.dbURL = dbUrl;
		this.dbUsername = username;
		this.dbPassword = password;
		
		ds = setupDataSource();
	}
		
	public DataSource setupDataSource() {
		BasicDataSource ds = new BasicDataSource();
	    ds.setUsername(this.dbUsername);
	    ds.setPassword(this.dbPassword);
	    ds.setUrl(this.dbURL);
	    ds.setDriverClassName("com.mysql.jdbc.Driver");
	    return ds;
	}

	public Project insertProject(Project proj) throws Exception{
		Connection conn = ds.getConnection();
		String insert = "INSERT INTO project(pname, description) VALUES(?, ?)";
		PreparedStatement stmt = conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, proj.getName());
		stmt.setString(2, proj.getDescription());
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating project failed, no rows affected.");
        }
		
        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next()) {
        	proj.setPID(generatedKeys.getInt(1));
        }
        else {
            throw new SQLException("Creating project failed, no ID obtained.");
        }
        
        stmt.close();
        // Close the connection
        conn.close();
        
		return proj;
	}
    public void updateProject(Project proj) throws SQLException{
    	Connection conn = ds.getConnection();
		String update = "UPDATE project set pname=?, description=? where pid=?";
		PreparedStatement stmt = conn.prepareStatement(update);
		stmt.setString(1, proj.getName());
		stmt.setString(2, proj.getDescription());
		stmt.setInt(3, proj.getPID());
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) 
            throw new SQLException("Updating project failed, no rows affected.");
        
        stmt.close();
        // Close the connection
        conn.close();
    }
    public Meeting insertMeeting(int projectId, Meeting m) throws SQLException{
    	Connection conn = ds.getConnection();
    	String insert = "INSERT INTO meeting(pid, mname, year) VALUES(?, ?, ?)";
    	PreparedStatement stmt = conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
    	stmt.setInt(1, projectId);
    	stmt.setString(2, m.getName());
		stmt.setInt(3, m.getYear());
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating meeting failed, no rows affected.");
        }
        
        stmt.close();
        // Close the connection
        conn.close();
        
    	return m;
    }
    public Project getProjectDetails(int projectId) throws SQLException{
    	Project p = new Project();
    	Connection conn = ds.getConnection();
    	String get = "select pid, pname, description from project where pid=?";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setInt(1,projectId);
    	ResultSet rs = stmt.executeQuery();
    	if(rs.next()){
    		p.setPID(rs.getInt(1));
    		p.setName(rs.getString(2));
    		p.setDescription(rs.getString(3));
    	}
    	else{
    		stmt.close();
    		conn.close();
    		return null;
    	}
		
		get = "select mname, year from meeting where pid=?";
		stmt = conn.prepareStatement(get);
		stmt.setInt(1,projectId);
		rs = stmt.executeQuery();
		ArrayList<Meeting> meetings = new ArrayList<Meeting>();
		while(rs.next()){
			Meeting m = new Meeting();
			m.setName(rs.getString(1));
			m.setYear(rs.getInt(2));
			meetings.add(m);
		}
		
		p.setMeetings(meetings);
		
		stmt.close();
		conn.close();
		
    	return p;
    }
    public boolean deleteProject(int projectId) throws SQLException{
    	boolean status = true;
    	Connection conn = ds.getConnection();
    	String delete = "DELETE FROM project WHERE pid=?";
    	PreparedStatement stmt = conn.prepareStatement(delete);
    	stmt.setInt(1,projectId);
    	int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) 
            status = false;
        
        stmt.close();
        // Close the connection
        conn.close();
        
        return status;
    }
	
}
