package assign.services;

import assign.domain.Meeting;
import assign.domain.Project;

public interface Project5Services {

        public Project insertProject(Project proj) throws Exception;
        public void updateProject(Project proj) throws Exception;
        public Meeting insertMeeting(int projectId, Meeting m) throws Exception;
        public Project getProjectDetails(int projectId) throws Exception;
        public boolean deleteProject(int projectId) throws Exception;

}