package assign.resources;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class EavesdropProjectsApplication extends Application {
	
private Set<Object> singletons = new HashSet<Object>();
private Set<Class<?>> classes = new HashSet<Class<?>>();

public EavesdropProjectsApplication() {
}
@Override
public Set<Class<?>> getClasses() {
	classes.add(EavesdropProjectsResource.class);
return classes;
}
@Override
public Set<Object> getSingletons() {
		return singletons;
}
}