package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import assign.domain.Meeting;
import assign.domain.Project;
import assign.services.Project5Services;
import assign.services.Project5ServicesImpl;


@Path("/myeavesdrop")
public class EavesdropProjectsResource {
	
	Project5Services p5Service;
	String password;
	String username;
	String dburl;
	
	public EavesdropProjectsResource(@Context ServletContext servletContext) {		
		dburl = servletContext.getInitParameter("DBURL");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		this.p5Service = new Project5ServicesImpl(dburl, username, password);
	}
	



@GET
@Path("/helloworld")
@Produces("text/html")
public String helloWorld() {
return "Hello world";
}

@POST
@Path("/projects")
@Consumes("application/xml")
public Response addProject(InputStream is) throws Exception{
	Project proj = readProject(is);
	if(!proj.getName().equals("") && !proj.getDescription().equals("")){
		proj = p5Service.insertProject(proj);
		return Response.created(URI.create("/projects/"+proj.getPID())).build();
	}
	else{
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
}

@PUT
@Path("/projects/{id}")
@Consumes("application/xml")
public Response updateProject(InputStream is, @PathParam("id") String pid) throws Exception{
	Project proj = readProject(is);
	proj.setPID(Integer.parseInt(pid));
	if(!proj.getName().equals("") && !proj.getDescription().equals("")){
		p5Service.updateProject(proj);
		return Response.status(Response.Status.OK).build();
	}
	else{
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
}

@POST
@Path("/projects/{id}/meetings")
@Consumes("application/xml")
public Response addMeeting(InputStream is, @PathParam("id") String pid) throws Exception{
	Meeting m = readMeeting(is);
	if(!m.getName().equals("") && !(m.getYear() == 0)){
		m = p5Service.insertMeeting(Integer.parseInt(pid),m);
		return Response.created(URI.create("/projects/"+pid+"/meetings")).build();
	}
	else{
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
}

@GET
@Path("/projects/{id}")
@Produces("application/xml")
public Response getProject(@PathParam("id") String pid) throws Exception{
	final Project project = p5Service.getProjectDetails(Integer.parseInt(pid));
	if(project != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputProject(outputStream, project);
			}
		};
		return Response.ok(so).build();
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@DELETE
@Path("/projects/{id}")
//@Consumes("application/xml")
public Response deleteProject(InputStream is, @PathParam("id") String pid) throws Exception{
	if(p5Service.deleteProject(Integer.parseInt(pid)))
		return Response.status(Response.Status.OK).build();
	else
		return Response.status(Response.Status.NOT_FOUND).build();
}


protected void outputProject(OutputStream os, Project project) throws IOException {
	try {
		JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(project, os);
} catch (JAXBException jaxb) {
	jaxb.printStackTrace();
	throw new WebApplicationException();
}
}


private Project readProject(InputStream is) {
    Project proj = new Project();
    proj.setName("");
    proj.setDescription("");
    try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);
            Element root = doc.getDocumentElement();
            NodeList nodes = root.getChildNodes();
            for (int i=0; i<nodes.getLength(); i++) {
                    Element element = (Element) nodes.item(i);
                    if (element.getTagName().equals("name"))
                    	proj.setName(element.getTextContent());
                    else if (element.getTagName().equals("description"))
                    	proj.setDescription(element.getTextContent());
            }
            return proj;
    }
    catch (Exception e) {
            throw new WebApplicationException(e,Response.Status.BAD_REQUEST);
    }
}


private Meeting readMeeting(InputStream is) {
    Meeting m = new Meeting();
    m.setName("");
    m.setYear(0);
    try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);
            Element root = doc.getDocumentElement();
            NodeList nodes = root.getChildNodes();
            for (int i=0; i<nodes.getLength(); i++) {
                    Element element = (Element) nodes.item(i);
                    if (element.getTagName().equals("name"))
                    	m.setName(element.getTextContent());
                    else if (element.getTagName().equals("year"))
                    	m.setYear(Integer.parseInt(element.getTextContent()));
            }
            return m;
    }
    catch (Exception e) {
            throw new WebApplicationException(e,Response.Status.BAD_REQUEST);
    }
}


}