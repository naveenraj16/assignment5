package assign.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="meeting")
public class Meeting {
        String name;
        int year;

        public Meeting () {

        }


        @XmlElement(name="name")
        public String getName() {
                return name;
        }

        public void setName(String mName) {
                this.name = mName;
        }

        @XmlElement(name="year")
        public int getYear() {
                return year;
        }

        public void setYear(int year) {
                this.year = year;
        }

}