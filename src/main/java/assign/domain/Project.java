package assign.domain;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="project")
@XmlType(propOrder={"name","description","meetings"})
public class Project {

  private int pID;
  private String name;
  private String description;
  private ArrayList<Meeting> meetings;

  public Project() {

  }


  @XmlAttribute(name="pid")
  public int getPID() {
          return pID;
  }

  @XmlElement(name="name")
  public String getName() {
          return name;
  }

  @XmlElement(name="description")
  public String getDescription() {
          return description;
  }

  @XmlElement(name="meeting")
  @XmlElementWrapper(name="meetings")
  public ArrayList<Meeting> getMeetings() {
          return meetings;
  }

  public void setPID(int i) {
          pID = i;
  }

  public void setName(String n) {
          name = n;
  }

  public void setDescription(String d) {
          description = d;
  }


  public void setMeetings(ArrayList<Meeting> m) {
          meetings = m;
  }

}
